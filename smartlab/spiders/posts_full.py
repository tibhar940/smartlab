import re
import requests
from lxml import html
import scrapy
from smartlab.items import PostItemFull, CommentItem


def parse_ids():
    """parse id's on the main page to crawl with default params (r1, r2)"""
    page = requests.get("https://smart-lab.ru")
    tree = html.fromstring(page.content)
    urls = tree.xpath(
        "//div[starts-with(@id, $val1)]/div[starts-with(@class, $val2)]/h2[starts-with(@class, val3)]/a/@href",
        val1="content", val2="topic", val3="title"
    )
    ids = [int(re.findall("\d+", url)[0]) for url in urls if re.search("\d+", url)]
    return ids


class PostsSpider(scrapy.Spider):
    name = 'posts_full'

    def start_requests(self):
        # define min (r1) and max (r2) id for crawl
        ids = parse_ids()

        # obtain param from command line
        r1 = getattr(self, 'r1', min(ids))
        r2 = getattr(self, 'r2', max(ids))

        urls = ['https://smart-lab.ru/blog/{0}.php'.format(i) for i in range(int(r1), int(r2))]
        for url in urls:
            yield scrapy.Request(url, self.parse)

    def parse(self, response):
        topic = response.xpath("//div[starts-with(@class, $val1)]", val1="topic")
        item = PostItemFull()
        item['title'] = ' '.join(topic.xpath('h1[@class=$val]//text()', val="title").getall())
        item['author'] = topic.xpath('ul[starts-with(@class, $val1)]/*/li[starts-with(@class, $val2)]/a[starts-with(@class, $val3)]/text()', val1="action", val2="author", val3="trader").get().strip()
        item['author_url'] = topic.xpath('ul[starts-with(@class, $val1)]/*/li[starts-with(@class, $val2)]/a[starts-with(@class, $val3)]/@href', val1="action", val2="author", val3="trader").get()
        item['dt'] = topic.xpath('ul[starts-with(@class, $val1)]/*/li[starts-with(@class, $val2)]/text()', val1="action", val2="date").get().strip()
        item['url'] = response.url
        item['text'] = '\n'.join(topic.xpath('div[@class=$val]//text()', val="content").getall())
        item['urls'] = topic.xpath('div[@class=$val]//@href', val="content").getall()
        item['rating'] = topic.xpath('ul[starts-with(@class, $val1)]/li[@class=$val2]/a/text()', val1="voting", val2="total").get().strip()
        item['tags'] = topic.xpath('ul[@class="tags"]/li/a/text()').getall()

        comments = response.xpath("//div[starts-with(@class, $val1)]", val1="comment ")
        comments_ = []
        for comment in comments[:]:
            citem = CommentItem()
            citem['ts'] = comment.xpath("div[@class=$val1]//li[@class=$val2]/text()", val1="info", val2="date").get()
            citem['nickname'] = comment.xpath("div[@class=$val1]//a[starts-with(@class, $val2)]/text()", val1="info", val2="author").get()
            citem['nickname_url'] = comment.xpath("div[@class=$val1]//a[starts-with(@class, $val2)]/@href", val1="info", val2="author").get()
            citem['text'] = ' '.join(comment.xpath("div[starts-with(@class, $val1)]//div[@class=$val2]/text()", val1="content", val2="text").getall())
            #citem['rating'] = comment.xpath("div[starts-with(@class, $val1)]/div[@class=$val2]/a/text()", val1="voting", val2="total").get().strip() TODO
            citem['urls'] = comment.xpath("div[starts-with(@class, $val1)]//div[@class=$val2]//@href", val1="content", val2="text").getall()
            comments_.append(citem)
        item['comments'] = comments_
        yield item
