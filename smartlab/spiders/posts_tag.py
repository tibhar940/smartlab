import scrapy
from smartlab.items import PostItem

class PostsSpider(scrapy.Spider):
    name = 'posts_tag'
    main = 'https://smart-lab.ru'

    def start_requests(self):
        url = 'https://smart-lab.ru/tag/{0}/page1'
        tag = getattr(self, 'tag', None)
        if tag is not None:
            url = url.format(tag)
        yield scrapy.Request(url, self.parse)

    def parse(self, response):

        for quote in response.xpath("//div[starts-with(@class, $val)]", val="topic bluid")[:]:

            item = PostItem()
            item['title'] = quote.xpath('h2[starts-with(@class, $val)]/a/text()', val="title").get().strip()
            item['author'] = quote.xpath('ul[starts-with(@class, $val1)]/*/li[starts-with(@class, $val2)]/a[starts-with(@class, $val3)]/text()', val1="action", val2="author", val3="trader").get().strip()
            item['dt'] = quote.xpath('ul[starts-with(@class, $val1)]/*/li[starts-with(@class, $val2)]/text()', val1="action", val2="date").get().strip()
            item['url'] = 'https://smart-lab.ru' + quote.xpath('h2[starts-with(@class, $val)]/a/@href', val="title").get().strip()
            item['tags'] = quote.xpath('ul[@class="forum_tags"]/li/a/text()').getall()[1:]
            try:
                item['text_preview'] = quote.xpath('div[@class=$val]', val="content").get().strip()
            except:
                item['text_preview'] = ''
            try:
                item['rating'] = quote.xpath('ul[starts-with(@class, $val1)]/li[@class=$val2]/a/text()', val1="voting  guest ", val2="total").get().strip()
            except:
                item['rating'] = ''
            yield item
        next_page = response.xpath('//div[@class="pagination1"]/a[@class=$val1]/@href', val1="page gradient last").getall()[-1]
        if next_page is not None:
            yield response.follow('https://smart-lab.ru' + next_page, self.parse)
