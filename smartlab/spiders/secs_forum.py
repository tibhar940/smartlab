import re
import requests
from lxml import html
import scrapy
from smartlab.items import ForumCommentItem


def parse_names():
    """parse names for secs forums"""
    page = requests.get("https://smart-lab.ru/forum")
    tree = html.fromstring(page.content)
    urls = tree.xpath("//div[starts-with(@class, $val1)]//a/@href", val1="kompanii_company_index")
    names = [url.split("/")[-1] for url in urls if "forum" in url]
    return names


class ForumSpider(scrapy.Spider):
    name = 'secs_forum'

    def start_requests(self):
        names_default = ",".join(parse_names())
        names = getattr(self, 'names', names_default).split(',')
        urls = ['https://smart-lab.ru/forum/{0}/page1/'.format(n.strip()) for n in names]
        for url in urls:
            yield scrapy.Request(url, self.parse)

    def parse(self, response):
        securityid = response.url.split('/')[-3]
        comments = response.xpath("//li[starts-with(@class, $val1)]", val1="cm_wrap bluid_")
        for comment in comments[:]:

            item = ForumCommentItem()
            item['securityid'] = securityid
            item['nickname'] = comment.xpath(
                "div[@class=$val1]//a[starts-with(@class, $val2)]/text()", val1="cmt_body", val2="a_name"
            ).get()
            item['nickname_url'] = comment.xpath(
                "div[@class=$val1]//a[starts-with(@class, $val2)]/@href", val1="cmt_body", val2="a_name"
            ).get()
            item['ts'] = comment.xpath("div[@class=$val1]//time/@datetime", val1="cmt_body").get()
            item['text'] = comment.xpath("div[@class=$val1]//div[@class=$val2]//text()", val1="cmt_body", val2="text").get()
            item['urls'] = comment.xpath("div[@class=$val1]//div[@class=$val2]//@href", val1="cmt_body", val2="text").getall()
            item['rating'] = comment.xpath(
                "div[@class=$val1]//a[starts-with(@class, $val2)]/text()", val1="cmt_body", val2="cm_mrk"
            ).get()
            yield item
        next_page = response.xpath('//div[@class="pagination1"]/a[@class=$val1]/@href', val1="page gradient last").getall()[-1]
        if next_page is not None:
            yield response.follow('https://smart-lab.ru' + next_page, self.parse)
