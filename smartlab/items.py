# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item, Field


class PostItem(Item):
    title = Field()
    url = Field()
    dt = Field()
    author = Field()
    text_preview = Field()
    rating = Field()
    tags = Field()


class ForumCommentItem(Item):
    securityid = Field()
    nickname = Field()
    nickname_url = Field()
    ts = Field()
    text = Field()
    urls = Field()
    rating = Field()


class PostItemFull(Item):
    title = Field()
    url = Field()
    dt = Field()
    author = Field()
    author_url = Field()
    text = Field()
    urls = Field()
    rating = Field()
    tags = Field()
    comments = Field()


class CommentItem(Item):
    nickname = Field()
    nickname_url = Field()
    ts = Field()
    text = Field()
    urls = Field()
    rating = Field()
