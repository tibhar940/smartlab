# smartlab
`https://smart-lab.ru/` wrapper based on Scrapy

## Spiders list
- `posts` (stable) - obtain posts (only titles) from main page
- `posts_full` (stable) - obtain posts (titles + text) from main page
- `secs_forum` - obtain posts from security-specified pages
- `posts_tag` - obtain posts (only titles) from main page by id

## Install dependencies
```shell
pip install -r requirements.txt
```

## How to
### Obtain spiders list
```shell
scrapy list
```

### Minimal examples
```shell
# posts
scrapy crawl posts -o data/posts.jl --nolog 

# posts_full
scrapy crawl posts_full -o data/posts_full.jl --nolog

# posts_tag
scrapy crawl posts_tag -a tag=SBER -o data/posts_tag.jl --nolog
```
