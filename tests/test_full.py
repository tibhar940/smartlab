import time
import logging
import pandas
import scrapy
import requests
import re
from lxml import html
#import json_lines
#from smartlab.items import ForumCommentItem
from scrapy.item import Item, Field
from scrapy.crawler import CrawlerProcess
from smartlab.spiders.posts_full import PostsSpider

logging.getLogger('scrapy').propagate = False

process = CrawlerProcess({'USER_AGENT': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', "FEEDS": {'posts.jl': {"format": "jl"}}})
process.crawl(PostsSpider)
process.start()
time.sleep(1.0)
process.stop()


#df_full = pandas.read_json('/content/full.jl', lines=True)
#df_full
